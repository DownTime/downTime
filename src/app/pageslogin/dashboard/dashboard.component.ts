import {Component, OnDestroy, OnInit} from '@angular/core';

import { Router } from '@angular/router';
import {AuthService} from "../../auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';


@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    alluser = [];
    text = "test";
    loginForm: FormGroup;
    isSubmitted: boolean = false;
    constructor(private Auth: AuthService,
                private router: Router,
                private toasterService: ToasterService) { }

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    ngOnInit() {
        // localStorage.clear();
        this.Auth.search_user().subscribe(data => {
            this.alluser = data.data;
        })
        let emp_data = localStorage.getItem('emp_data');
        if (emp_data == '1') {
            this.router.navigate(['/pages']);
        }

        this.loginForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10),
                Validators.pattern('[0-9 ]*')
            ]),
            password: new FormControl('', [
                Validators.required,
                // Validators.pattern('[a-zA-Z ]*')
            ])
        })

    }

    loginUser(event) {
        this.isSubmitted = true
        if (this.loginForm.valid == true) {
            this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
            event.preventDefault()
            const target = event.target
            const username = target.querySelector('#username').value
            const password = target.querySelector('#password').value
            this.Auth.loginUser(username, password).subscribe(data => {
                if (data.success) {
                    this.showToast('success', 'เข้าสู่ระบบสำเร็จ', 'กำลังทำการเข้าสู่ระบบ');
                    switch (data.user_status) {
                        case 1 : {
                            console.log(1);
                            localStorage.setItem('emp_data', data.emp_data);
                            if (data.emp_data == '0') {
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['/pages']);
                            }
                            break;
                        }
                        case 2 : {
                            this.router.navigate(['/pages']);
                            console.log(2);
                            localStorage.setItem('emp_data', data.emp_data);
                            if (data.emp_data == '0') {
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['/pages']);
                            }
                            break;
                        }
                        case 3 : {
                            this.router.navigate(['/manager']);
                            console.log(3);
                            break;
                        }
                        case 4 : {
                            console.log(4);
                            this.router.navigate(['/admin']);
                            break;
                        }
                        default : {
                            this.router.navigate(['/login']);
                            console.log(5);
                            break;
                        }
                    }
                    localStorage.setItem('username', username);
                    localStorage.setItem('password', password);
                    localStorage.setItem('emp_id', data.emp_id);
                    /*this.Auth.getEmployee(username, password).subscribe(data2 => {
                        if (data2.emp_data == '0') {
                            this.router.navigate(['pages/employee/one'])
                        }else{
                            this.router.navigate([''])
                        }
                        localStorage.setItem('emp_data', data2.emp_data);
                    })*/
                    // this.router.navigate([''])
                    this.Auth.setLoggedIn(true)
                } else {
                    this.showToast('error', 'ไอดีหรือรหัสผ่านผิด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
                    this.isSubmitted = false;
                    localStorage.setItem('username', username);
                    localStorage.setItem('password', password);
                    localStorage.setItem('emp_id', data.emp_id);
                    // window.alert(data.message)
                }
                localStorage.setItem('user_status', data.user_status.toString());
                localStorage.setItem('loginSuccess', data.success);
                let myItem = localStorage.getItem('loginSuccess');
                console.log('loginSuccess => ', myItem);
            })
            console.log(username, password)
        }else {
            this.showToast('error', 'ผิดพลาด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
        }
    }

}
