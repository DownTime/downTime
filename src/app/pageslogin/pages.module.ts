import { NgModule } from '@angular/core';

import { ToasterModule } from 'angular2-toaster';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { ProfileComponent } from './profile/profile.component';
// import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    // MiscellaneousModule,
      ToasterModule.forRoot(),
  ],
  declarations: [
    ...PAGES_COMPONENTS,
      ProfileComponent,
  ],
})
export class PagesModule {
}
