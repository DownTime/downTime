import { Component } from '@angular/core';

@Component({
  selector: 'ngx-submenu3',
  styleUrls: ['./submenu3.component.scss'],
  templateUrl: './submenu3.component.html',
})
export class Submenu3Component {

    iconToolbarModel = {
        one: false,
        two: false,
        three: true,
        four: false,
        five: false,
        six: false,
        seven: false,
        eight: false,
        nine: false,
        ten: false,
    };

    Employees = ['Han Solo', 'Luke Skywalker', 'Jedi', 'Obi Wan', 'Dark Vader', 'Yoda', 'R2-D2'];
    EmployeesTest = [{emp_name: 'Han Solo', iconToolbar: this.iconToolbarModel}];
}
