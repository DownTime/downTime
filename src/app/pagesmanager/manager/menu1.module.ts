import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { Menu1RoutingModule, routedComponents } from './menu1-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    Menu1RoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class Menu1Module { }
