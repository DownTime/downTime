import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'manager',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'รายชื่อพนักงาน',
                link: '/manager/manager/submenu1',
            },
            {
                title: 'ให้ดาว',
                link: '/manager/manager/submenu2',
            },
            {
                title: 'ตั่งค่าแทค',
                link: '/manager/manager/submenu3',
            },
            {
                title: 'Report',
                link: '/manager/manager/submenu4',
            },
            {
                title: 'upload',
                link: '/manager/manager/submenu5',
            },
            /*{
                title: 'submenu5',
                link: '/pages/manager/submenu5',
            },*/
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/logout',
        home: true,
    },
];
