import { Component } from '@angular/core';

@Component({
  selector: 'ngx-menu1-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class Menu1Component {
}
