import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'Employee',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'ข้อมูลส่วนตัว',
                link: '/pages/employee/proflie',
            },
            {
                title: 'ลงเวลาทำงาน',
                link: '/pages/employee/downtime',
            },
            {
                title: 'ลงคนแทนงาน',
                link: '/pages/employee/delegate',
            },
            {
                title: 'ออกรายงาน',
                link: '/pages/employee/report',
            },
            // {
            //     title: 'เข้าสู่ระบบครั้งแรก',
            //     link: '/pages/employee/one',
            // },
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/logout',
        home: true,
    },
];
