import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../../auth.service';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./submenu1.component.scss'],
  templateUrl: './submenu1.component.html',
})
export class Submenu1Component implements OnInit {

    constructor(private Auth: AuthService ) {}
    users = []
    user = {}
    starRate: number = 1;
    starRate2: number = 5;
    starRate3: number = 1;
    starRate4: number = 2;
    ;
    num: number = 4;

    sumRate: number = this.starRate + this.starRate2 + this.starRate3 + this.starRate4;
    avestarRate: number;

    hostUrl = "http://localhost/scode_downTime/";
    // hostUrl = "http://scodedev.com/laravel/";
    ngOnInit() {
        let emp_id = localStorage.getItem('emp_id');
        this.Auth.search_oneuser(emp_id).subscribe(data => {
            this.users = data.users;
        })
        this.avestarRate = this.starRate + this.starRate2 + this.starRate3 + this.starRate4;
        console.log ("avestarRate => ", this.avestarRate);
        this.avestarRate = this.sumRate / this.num;
    }

    changeStar() {
        this.avestarRate = this.starRate + this.starRate2 + this.starRate3 + this.starRate4;
        console.log ("avestarRate => ", this.avestarRate);
    }

}
