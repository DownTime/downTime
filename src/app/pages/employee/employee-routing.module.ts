import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';
import { Submenu1Component } from './submenu1/submenu1.component';
import { Submenu2Component } from './submenu2/submenu2.component';
import { Submenu3Component } from './submenu3/submenu3.component';
import { Submenu4Component } from './submenu4/submenu4.component';
import { Submenu5Component } from './submenu5/submenu5.component';

const routes: Routes = [{
  path: '',
  component: EmployeeComponent,
  children: [{
    path: 'proflie',
    component: Submenu1Component,
  }, {
    path: 'downtime',
    component: Submenu2Component,
  }, {
      path: 'delegate',
      component: Submenu3Component,
  }, {
      path: 'report',
      component: Submenu4Component,
  }, {
      path: 'one',
      component: Submenu5Component,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class EmployeeRoutingModule {

}

export const routedComponents = [
  EmployeeComponent,
    Submenu1Component,
    Submenu2Component,
    Submenu3Component,
    Submenu4Component,
    Submenu5Component,
];
