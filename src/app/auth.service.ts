import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface myData {
    success: string,
    message: string,
    data: any[],
    user_status: number,
    emp_id: string,
    emp_data: string,
    user: any[],
    admin: any[],
    status: string,
    users: any[],
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false
    hostUrl = 'http://127.0.0.1:8000';
    // hostUrl = 'http://scodedev.com/laravel/public';
  constructor(private http: HttpClient) { }

    setLoggedIn(value: boolean) {
        this.loggedInStatus = value
    }

    get isLoggedIn() {
        return this.loggedInStatus
    }
    // สแกนลายนิ้วมือ
    /*getuploadFin(folder) {
        // return this.http.post<myData>('http://localhost/test/txt/uploadFin.php', {
        return this.http.post<myData>('http://scodedev.com/test/txt/uploadFin.php', {
            folder
        })
    }*/

    /*getuploadFin2(folder) {
        // return this.http.post<myData>('http://localhost/test/txt/uploadFin.php', {
            return this.http.post<myData>('http://scodedev.com/test/txt/uploadFin.php', {
            folder
        })
    }*/
    search_user() {
        return this.http.post<myData>(this.hostUrl + '/api/search_user', {
        })
    }

    search_oneuser(emp_id) {
        return this.http.post<myData>(this.hostUrl + '/api/search_oneuser', {
            emp_id
        })
    }

    search_manager() {
        return this.http.post<myData>(this.hostUrl + '/api/search_manager', {
        })
    }
    alluser() {
        return this.http.post<myData>(this.hostUrl + '/api/alluser', {
        })
    }

    loginUser( username, password ) {
        return this.http.post<myData>(this.hostUrl + '/api/loginUser', {
            username,
            password
        })
    }

    AddUser( emp_id, user_id, user_pass, user_status) {
        return this.http.post<myData>(this.hostUrl + '/api/adduser', {
            emp_id, user_id, user_pass, user_status
        })
    }
    AddManager(user_id, user_pass) {
        return this.http.post<myData>(this.hostUrl + '/api/addmanager', {
            user_id, user_pass
        })
    }

    EditUser( emp_id, user_id, user_pass, user_status) {
        return this.http.post<myData>(this.hostUrl + '/api/edit_user', {
            emp_id, user_id, user_pass, user_status
        })
    }

    DeleteUser(emp_id) {
        return this.http.post<myData>(this.hostUrl + '/api/delete_user', {
            emp_id
        })
    }

    UpdateRegister(inputs) {
        return this.http.post<myData>(this.hostUrl + '/api/update_register', {
            inputs
        })
    }

    all_register() {
        return this.http.post<myData>(this.hostUrl + '/api/all_register', {
        })
    }

    UploadFile(fd) {
      console.log ('file inout', fd);
        return this.http.post<myData>(this.hostUrl + '/api/uploadfile', {
            fd
        })
    }

    testUpload(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/testUpload', formData)
    }
    UploadImg(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/UploadImg', formData)
    }
}
